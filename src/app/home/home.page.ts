import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { ToastController } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpParams  } from  '@angular/common/http';




@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  qrcode;
  nama:string = '';
  Perusahaan:string = '';
  NoMeja:number;
  found:boolean = false;
  qrData= 'www.dapenastra.com'; //default
  scannedCode = null;
  elementType: 'url' | 'canvas' | 'img' = 'canvas';
  // base_url = 'https://10.1.25.28/absensip3/'; //ganti alamat web sesuai kebutuhan
  base_url= 'https://10.1.25.198/absensip3/'; //ganti alamat web sesuai kebutuhan
  url= this.base_url+'Attendance/myQRCode';
  url2= this.base_url+'Attendance/getQRData';

  constructor(private barcodeScanner: BarcodeScanner,private base64ToGallery: Base64ToGallery,
    private toastCtrl: ToastController, private http: HttpClient) {

    }

  scanCode() {
    
    this.barcodeScanner.scan().then(
      barcodeData => {
        this.scannedCode = barcodeData.text;
        const body = new HttpParams()
        .set('qrcode',this.scannedCode);
        this.http.post(this.url,body,{headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')}).subscribe({
          next: data => this.qrcode = this.scannedCode,
          error: error => console.error('There was an error!', error)
        });
        this.http.get(this.url2+'/'+this.scannedCode)
        .subscribe(
          (data: any[]) => {
            console.log(data);
            if(data.length){
              this.nama = data[0].NmPeserta;
              this.Perusahaan = data[0].Perusahaan;
              this.NoMeja = data[0].NoMeja;
              this.found = true;
              console.log(this.nama, this.Perusahaan, this.NoMeja, this.found);
            }
          }
        )
      }
      
    );

    let postData = {
      "qrdata": this.scannedCode
    }
    // this.http.post(this.url, { title: 'qrcode' }).subscribe({
    //   next: data => this.scannedCode,
    //   error: error => console.error('There was an error!', error)
    // })
  }

  downloadQR() {
    const canvas = document.querySelector('canvas') as HTMLCanvasElement;
    const imageData = canvas.toDataURL('image/jpeg').toString();
    console.log('data: ', imageData);

    let data = imageData.split(',')[1];

    this.base64ToGallery.base64ToGallery(data,
      { prefix: '_img',mediaScanner: true })
      .then(async res =>{
          let toast = await this.toastCtrl.create({
            header: 'QR Code save in your Photolibrary'
          });
          toast.present();
        }, err => console.log('err: ',err)
      );
  }

}
